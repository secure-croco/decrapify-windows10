# Automatically Decrapify Windows 10

## Purpose

Make Windows 10 cleaner and less invasive, while minimizing the risks of breaking things.

## What the Script Does

### App Cleanup

1. Clear all the start menu tiles *(for future users only)*.
1. Disable app suggestions.
Windows 10 will no longer offer new apps from the start menu.
1. Disable silent app install.
Windows 10 will no longer install new apps without asking the users.
1. Remove all apps deemed unnecessary. You may want to review the following files before running the script to customize what gets removed:
    - [unwanted_default_apps.txt](content/unwanted_default_apps.txt)
    - [unwanted_extra_apps.txt](content/unwanted_extra_apps.txt)

### Privacy Settings

Tighten the privacy settings of Windows 10. This part relies on a tool called **ShutUp10** by [O&O Software](https://www.oo-software.com).

The tool will be automatically installed and will run in the background everytime you log on to Windows, to ensure the stricter privacy settings remain applied over time *(Windows Update has a tendency to reset some privacy settings)*.

The default privacy settings applied by this script may be too restrictive for your context. You may consider building your own custom settings:
1. From `Start Menu > Run`, you can launch `oosu10.exe` and customize your settings using the graphical user interface.
1. Export your finalized settings and overwrite [shutup10_template.cfg](content/shutup10_template.cfg).
1. Re-run this script to register your new settings.

## How to Use

1. Open **Command Prompt** or **PowerShell** as admin.
1. Execute `run.bat`
1. That's it!

## Compatibility

Tested with **Windows 10 Pro, build 20H2**.

## License

Please see the [LICENSE](LICENSE) file.

## Reference Material

- https://docs.microsoft.com/en-us/windows/application-management/apps-in-windows-10
- https://blog.danic.net/provisioned-app-packages-in-windows-10-enterprise-windows-10-pro/
- https://vacuumbreather.com/index.php/blog/item/87-windows-10-1903-built-in-apps-what-to-keep

## Logo Attribution

<div>Icons made by <a href="https://www.flaticon.com/authors/freepik" title="Freepik">Freepik</a> from <a href="https://www.flaticon.com/" title="Flaticon">www.flaticon.com</a></div>
