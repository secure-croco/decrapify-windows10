function Disable-SuggestedApps()
{
    $path = "HKCU:\Software\Microsoft\Windows\CurrentVersion\ContentDeliveryManager"
    Set-ItemProperty -Path $path -Name "SystemPaneSuggestionsEnabled" -Value 0
    "Disabled suggested apps from the Windows Store." | Write-Host -ForegroundColor Green
}

function Disable-SilentAppInstall()
{
    $path = "HKCU:\Software\Microsoft\Windows\CurrentVersion\ContentDeliveryManager"
    Set-ItemProperty -Path $path -Name "SilentInstalledAppsEnabled" -Value 0
    "Disabled silent app install from the Windows Store." | Write-Host -ForegroundColor Green
}

Disable-SuggestedApps
Disable-SilentAppInstall
